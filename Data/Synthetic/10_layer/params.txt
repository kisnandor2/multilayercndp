spread_critical_nodes = True,
number_of_layers = 10,
min_number_of_nodes_on_each_layer = 6,
max_number_of_nodes_on_each_layer = 12,
number_of_critical_nodes_in_total = 5,
max_number_of_critical_nodes_on_each_layer = 1


chance_in_percent_of_connecting_two_nodes = 0.5