# MultilayerCNDP

A project for UBB about multilayered network critical node detection problem

# Some acronyms

- G - graph = (V, E)
- V - set of nodes/vertices
- S - part of V - nodes that will be deleted
- σ - connectivity metric
- f(σ, G) - Target function -> function to be optimized - the σ metric applied to the G
- CNDP - critical node detection problem

# K-vertex-CNDP
- No more than K nodes (K = |S|) to be deleted such that our target function is optimized (minimized or maximized)

- useful when we have some information about the number of nodes to target

## Ex of connectivity metrics.
- (minimize the) largest component size
- (maximize the) number of small components
- (minimize the) number of large components
- (maximize the) number of connected components
- Critical Node Problem - (minimize the) pariwise connectivity (by deleting K nodes)


# β-connectivity-CNDP
- Minimize the number of deleted nodes such as our target function is bound to β (f(σ, G) <= (**TODO - check**) β)

- useful when we have an upper limit of the cost of eliminating nodes (**TODO- check**)

## Ex of connectivity metrics.
- won't put any examples here since we will focus on the K-vertex-CNDP
- probably same as above but with a threshold applied to the function and minimization of the K

# Ranking nodes by their importance
- not the same as CNDP - some metrics have nothing to do with the importance of a node (for ex. maximize the number of connected components) - or at least I could hardly prove that the two problems always have an identical solution
    
## Multilayer centrality measures:
- Usual random walker
- PageRank (random walk + teleportation)
- m-PageRank - 02. jo_1.pdf (pagerank + some tricks with teleportation + assign weights to layers and add them to the nodes => importance of node increases)
- SVT        - 03. 1-s2.0-S0307904X1730450X.pdf
