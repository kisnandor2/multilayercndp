#!/bin/bash

jupyter nbconvert --to script CriticalNodeByMOA.ipynb
jupyter nbconvert --to script MOATesting.ipynb

rm all.py 2&>/dev/null
touch all.py
chmod +x all.py

cat CriticalNodeByMOA.py >> all.py
cat MOATesting.py >> all.py

# Remove juptyer magic commands
sed -i '/get_ipython/d' all.py
# Remove comments
sed -i '/^[ ]*#/d' all.py
# Remove empty lines and trim right
sed -i 's/ *$//; /^$/d' all.py
# Add #!/usr/bin/python3 at the beginning
sed -i '1s/^/#!\/usr\/bin\/python3\n/' all.py

# Add this line so I don't forget to edit the output
echo "" >> all.py
echo "if __name__ == '__main__':" >> all.py

echo "Please edit the output with the main function!!"